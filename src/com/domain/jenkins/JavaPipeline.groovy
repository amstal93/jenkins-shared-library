package com.domain.jenkins

class JavaPipeline implements Serializable {

	/**
	 * Checks if the tools needed to run the pipeline are available in the pipeline
	 * execution environment.
	 */
	def static verifyTools() {
		sh """
            docker --version
            node --version
            npm --version
            mvn --version
            git --version
            jq --version
            semantic-release --version
		"""
	}
}
